﻿
namespace Labirint2D
{
    partial class FormLevel2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label2_start = new System.Windows.Forms.Label();
            this.label2_finish = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label_key = new System.Windows.Forms.Label();
            this.label_door = new System.Windows.Forms.Label();
            this.label_wall1 = new System.Windows.Forms.Label();
            this.label_wall2 = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label2_m = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label2_s = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2_start
            // 
            this.label2_start.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label2_start.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2_start.Location = new System.Drawing.Point(3, 10);
            this.label2_start.Name = "label2_start";
            this.label2_start.Size = new System.Drawing.Size(107, 42);
            this.label2_start.TabIndex = 0;
            this.label2_start.Text = "start";
            this.label2_start.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2_finish
            // 
            this.label2_finish.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label2_finish.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2_finish.Location = new System.Drawing.Point(544, 499);
            this.label2_finish.Name = "label2_finish";
            this.label2_finish.Size = new System.Drawing.Size(110, 42);
            this.label2_finish.TabIndex = 1;
            this.label2_finish.Text = "finish";
            this.label2_finish.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2_finish.MouseEnter += new System.EventHandler(this.label2_finish_MouseEnter);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Teal;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(665, 10);
            this.label1.TabIndex = 2;
            this.label1.MouseEnter += new System.EventHandler(this.label1_MouseEnter);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(0, 541);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(665, 10);
            this.label2.TabIndex = 3;
            this.label2.MouseEnter += new System.EventHandler(this.label1_MouseEnter);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Teal;
            this.label3.Location = new System.Drawing.Point(0, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(10, 546);
            this.label3.TabIndex = 4;
            this.label3.MouseEnter += new System.EventHandler(this.label1_MouseEnter);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Teal;
            this.label4.Location = new System.Drawing.Point(655, -3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(10, 554);
            this.label4.TabIndex = 5;
            this.label4.MouseEnter += new System.EventHandler(this.label1_MouseEnter);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Teal;
            this.label5.Location = new System.Drawing.Point(273, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(10, 170);
            this.label5.TabIndex = 6;
            this.label5.MouseEnter += new System.EventHandler(this.label1_MouseEnter);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Teal;
            this.label6.Location = new System.Drawing.Point(148, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 10);
            this.label6.TabIndex = 7;
            this.label6.MouseEnter += new System.EventHandler(this.label1_MouseEnter);
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Teal;
            this.label7.Location = new System.Drawing.Point(100, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(10, 313);
            this.label7.TabIndex = 8;
            this.label7.MouseEnter += new System.EventHandler(this.label1_MouseEnter);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Teal;
            this.label8.Location = new System.Drawing.Point(534, 276);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 10);
            this.label8.TabIndex = 9;
            this.label8.MouseEnter += new System.EventHandler(this.label1_MouseEnter);
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Teal;
            this.label9.Location = new System.Drawing.Point(532, 276);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(10, 265);
            this.label9.TabIndex = 10;
            this.label9.MouseEnter += new System.EventHandler(this.label1_MouseEnter);
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Teal;
            this.label10.Location = new System.Drawing.Point(148, 82);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(10, 240);
            this.label10.TabIndex = 11;
            this.label10.MouseEnter += new System.EventHandler(this.label1_MouseEnter);
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Teal;
            this.label11.Location = new System.Drawing.Point(183, 82);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(10, 94);
            this.label11.TabIndex = 12;
            this.label11.MouseEnter += new System.EventHandler(this.label1_MouseEnter);
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Teal;
            this.label12.Location = new System.Drawing.Point(183, 166);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 10);
            this.label12.TabIndex = 13;
            this.label12.MouseEnter += new System.EventHandler(this.label1_MouseEnter);
            // 
            // label_key
            // 
            this.label_key.BackColor = System.Drawing.Color.Yellow;
            this.label_key.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_key.Location = new System.Drawing.Point(199, 127);
            this.label_key.Name = "label_key";
            this.label_key.Size = new System.Drawing.Size(63, 39);
            this.label_key.TabIndex = 14;
            this.label_key.Text = "key";
            this.label_key.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_key.MouseEnter += new System.EventHandler(this.label_key_MouseEnter);
            // 
            // label_door
            // 
            this.label_door.BackColor = System.Drawing.Color.Yellow;
            this.label_door.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_door.Location = new System.Drawing.Point(581, 263);
            this.label_door.Name = "label_door";
            this.label_door.Size = new System.Drawing.Size(74, 35);
            this.label_door.TabIndex = 15;
            this.label_door.Text = "door";
            this.label_door.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_door.MouseEnter += new System.EventHandler(this.label_door_MouseEnter);
            // 
            // label_wall1
            // 
            this.label_wall1.BackColor = System.Drawing.Color.PaleTurquoise;
            this.label_wall1.Location = new System.Drawing.Point(3, 322);
            this.label_wall1.Name = "label_wall1";
            this.label_wall1.Size = new System.Drawing.Size(655, 40);
            this.label_wall1.TabIndex = 16;
            this.label_wall1.MouseEnter += new System.EventHandler(this.label1_MouseEnter);
            // 
            // label_wall2
            // 
            this.label_wall2.BackColor = System.Drawing.Color.PaleTurquoise;
            this.label_wall2.Location = new System.Drawing.Point(356, 9);
            this.label_wall2.Name = "label_wall2";
            this.label_wall2.Size = new System.Drawing.Size(42, 532);
            this.label_wall2.TabIndex = 17;
            this.label_wall2.MouseEnter += new System.EventHandler(this.label1_MouseEnter);
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 900;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label2_m
            // 
            this.label2_m.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label2_m.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2_m.Location = new System.Drawing.Point(506, 18);
            this.label2_m.Name = "label2_m";
            this.label2_m.Size = new System.Drawing.Size(52, 34);
            this.label2_m.TabIndex = 18;
            this.label2_m.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(565, 18);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(22, 34);
            this.label13.TabIndex = 19;
            this.label13.Text = ":";
            // 
            // label2_s
            // 
            this.label2_s.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label2_s.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2_s.Location = new System.Drawing.Point(593, 18);
            this.label2_s.Name = "label2_s";
            this.label2_s.Size = new System.Drawing.Size(52, 34);
            this.label2_s.TabIndex = 20;
            this.label2_s.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormLevel2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PeachPuff;
            this.ClientSize = new System.Drawing.Size(663, 548);
            this.ControlBox = false;
            this.Controls.Add(this.label2_s);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label2_m);
            this.Controls.Add(this.label_door);
            this.Controls.Add(this.label_key);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2_finish);
            this.Controls.Add(this.label2_start);
            this.Controls.Add(this.label_wall1);
            this.Controls.Add(this.label_wall2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormLevel2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Shown += new System.EventHandler(this.FormLevel2_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2_start;
        private System.Windows.Forms.Label label2_finish;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label_key;
        private System.Windows.Forms.Label label_door;
        private System.Windows.Forms.Label label_wall1;
        private System.Windows.Forms.Label label_wall2;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label2_m;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label2_s;
    }
}