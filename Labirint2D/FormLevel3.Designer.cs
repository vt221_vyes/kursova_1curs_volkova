﻿
namespace Labirint2D
{
    partial class FormLevel3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label3_start = new System.Windows.Forms.Label();
            this.label3_finish = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label3_wall3 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label3_wall1 = new System.Windows.Forms.Label();
            this.label3_wall2 = new System.Windows.Forms.Label();
            this.label3_key = new System.Windows.Forms.Label();
            this.label3_door = new System.Windows.Forms.Label();
            this.label3_box5 = new System.Windows.Forms.Label();
            this.label3_box1 = new System.Windows.Forms.Label();
            this.label3_box4 = new System.Windows.Forms.Label();
            this.label3_box2 = new System.Windows.Forms.Label();
            this.label3_box3 = new System.Windows.Forms.Label();
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label3_s = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label3_m = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(634, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(10, 502);
            this.label7.TabIndex = 9;
            this.label7.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(-4, 492);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(648, 10);
            this.label1.TabIndex = 10;
            this.label1.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(-2, -1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 502);
            this.label2.TabIndex = 11;
            this.label2.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(-2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(646, 10);
            this.label3.TabIndex = 12;
            this.label3.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label3_start
            // 
            this.label3_start.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label3_start.Location = new System.Drawing.Point(5, 9);
            this.label3_start.Name = "label3_start";
            this.label3_start.Size = new System.Drawing.Size(107, 42);
            this.label3_start.TabIndex = 13;
            this.label3_start.Text = "start";
            this.label3_start.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3_finish
            // 
            this.label3_finish.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label3_finish.Location = new System.Drawing.Point(523, 450);
            this.label3_finish.Name = "label3_finish";
            this.label3_finish.Size = new System.Drawing.Size(110, 42);
            this.label3_finish.TabIndex = 14;
            this.label3_finish.Text = "finish";
            this.label3_finish.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3_finish.MouseEnter += new System.EventHandler(this.label3_finish_MouseEnter);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(105, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(10, 196);
            this.label4.TabIndex = 15;
            this.label4.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(68, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(246, 10);
            this.label5.TabIndex = 16;
            this.label5.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(370, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(10, 178);
            this.label6.TabIndex = 17;
            this.label6.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(207, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(173, 10);
            this.label8.TabIndex = 18;
            this.label8.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(304, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(10, 51);
            this.label9.TabIndex = 19;
            this.label9.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(141, 143);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(173, 10);
            this.label10.TabIndex = 20;
            this.label10.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(138, -1);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(10, 154);
            this.label11.TabIndex = 21;
            this.label11.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(204, 372);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(110, 10);
            this.label12.TabIndex = 22;
            this.label12.Text = "я";
            this.label12.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(201, 372);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(10, 120);
            this.label13.TabIndex = 23;
            this.label13.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(539, 76);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(10, 178);
            this.label14.TabIndex = 24;
            this.label14.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(437, 249);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(196, 10);
            this.label15.TabIndex = 25;
            this.label15.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(513, 372);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(10, 120);
            this.label16.TabIndex = 26;
            this.label16.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(144, 249);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(170, 10);
            this.label17.TabIndex = 27;
            this.label17.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(144, 249);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(10, 123);
            this.label18.TabIndex = 28;
            this.label18.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(68, 194);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(10, 257);
            this.label19.TabIndex = 29;
            this.label19.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(304, 249);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(76, 10);
            this.label20.TabIndex = 30;
            this.label20.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label3_wall3
            // 
            this.label3_wall3.BackColor = System.Drawing.Color.Aqua;
            this.label3_wall3.Location = new System.Drawing.Point(304, 372);
            this.label3_wall3.Name = "label3_wall3";
            this.label3_wall3.Size = new System.Drawing.Size(10, 120);
            this.label3_wall3.TabIndex = 31;
            this.label3_wall3.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(456, -1);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(10, 197);
            this.label22.TabIndex = 32;
            this.label22.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(304, 244);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(10, 128);
            this.label23.TabIndex = 33;
            this.label23.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(370, 222);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(10, 98);
            this.label24.TabIndex = 34;
            this.label24.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(304, 372);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(150, 10);
            this.label25.TabIndex = 35;
            this.label25.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(523, 310);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(121, 10);
            this.label26.TabIndex = 36;
            this.label26.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label3_wall1
            // 
            this.label3_wall1.BackColor = System.Drawing.Color.Aqua;
            this.label3_wall1.Location = new System.Drawing.Point(304, 194);
            this.label3_wall1.Name = "label3_wall1";
            this.label3_wall1.Size = new System.Drawing.Size(10, 55);
            this.label3_wall1.TabIndex = 37;
            this.label3_wall1.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label3_wall2
            // 
            this.label3_wall2.BackColor = System.Drawing.Color.Aqua;
            this.label3_wall2.Location = new System.Drawing.Point(370, 249);
            this.label3_wall2.Name = "label3_wall2";
            this.label3_wall2.Size = new System.Drawing.Size(72, 10);
            this.label3_wall2.TabIndex = 38;
            this.label3_wall2.MouseEnter += new System.EventHandler(this.label2_MouseEnter);
            // 
            // label3_key
            // 
            this.label3_key.BackColor = System.Drawing.Color.Magenta;
            this.label3_key.Location = new System.Drawing.Point(204, 299);
            this.label3_key.Name = "label3_key";
            this.label3_key.Size = new System.Drawing.Size(63, 39);
            this.label3_key.TabIndex = 39;
            this.label3_key.Text = "key";
            this.label3_key.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3_key.MouseEnter += new System.EventHandler(this.label3_key_MouseEnter);
            // 
            // label3_door
            // 
            this.label3_door.BackColor = System.Drawing.Color.Magenta;
            this.label3_door.Location = new System.Drawing.Point(543, 76);
            this.label3_door.Name = "label3_door";
            this.label3_door.Size = new System.Drawing.Size(94, 35);
            this.label3_door.TabIndex = 40;
            this.label3_door.Text = "door";
            this.label3_door.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3_door.MouseEnter += new System.EventHandler(this.label3_door_MouseEnter);
            // 
            // label3_box5
            // 
            this.label3_box5.BackColor = System.Drawing.Color.Yellow;
            this.label3_box5.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3_box5.Location = new System.Drawing.Point(329, 279);
            this.label3_box5.Name = "label3_box5";
            this.label3_box5.Size = new System.Drawing.Size(29, 29);
            this.label3_box5.TabIndex = 41;
            this.label3_box5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3_box5.MouseEnter += new System.EventHandler(this.label3_box1_MouseEnter);
            // 
            // label3_box1
            // 
            this.label3_box1.BackColor = System.Drawing.Color.Yellow;
            this.label3_box1.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3_box1.Location = new System.Drawing.Point(86, 214);
            this.label3_box1.Name = "label3_box1";
            this.label3_box1.Size = new System.Drawing.Size(29, 29);
            this.label3_box1.TabIndex = 42;
            this.label3_box1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3_box1.MouseEnter += new System.EventHandler(this.label3_box1_MouseEnter);
            // 
            // label3_box4
            // 
            this.label3_box4.BackColor = System.Drawing.Color.Yellow;
            this.label3_box4.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3_box4.Location = new System.Drawing.Point(574, 186);
            this.label3_box4.Name = "label3_box4";
            this.label3_box4.Size = new System.Drawing.Size(29, 29);
            this.label3_box4.TabIndex = 43;
            this.label3_box4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3_box4.MouseEnter += new System.EventHandler(this.label3_box1_MouseEnter);
            // 
            // label3_box2
            // 
            this.label3_box2.BackColor = System.Drawing.Color.Yellow;
            this.label3_box2.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3_box2.Location = new System.Drawing.Point(238, 422);
            this.label3_box2.Name = "label3_box2";
            this.label3_box2.Size = new System.Drawing.Size(29, 29);
            this.label3_box2.TabIndex = 44;
            this.label3_box2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3_box2.MouseEnter += new System.EventHandler(this.label3_box1_MouseEnter);
            // 
            // label3_box3
            // 
            this.label3_box3.BackColor = System.Drawing.Color.Yellow;
            this.label3_box3.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3_box3.Location = new System.Drawing.Point(164, 32);
            this.label3_box3.Name = "label3_box3";
            this.label3_box3.Size = new System.Drawing.Size(29, 29);
            this.label3_box3.TabIndex = 45;
            this.label3_box3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3_box3.MouseEnter += new System.EventHandler(this.label3_box1_MouseEnter);
            // 
            // timer3
            // 
            this.timer3.Enabled = true;
            this.timer3.Interval = 900;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label3_s
            // 
            this.label3_s.BackColor = System.Drawing.Color.Transparent;
            this.label3_s.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3_s.ForeColor = System.Drawing.Color.White;
            this.label3_s.Location = new System.Drawing.Point(585, 10);
            this.label3_s.Name = "label3_s";
            this.label3_s.Size = new System.Drawing.Size(52, 34);
            this.label3_s.TabIndex = 46;
            this.label3_s.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(557, 9);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(22, 34);
            this.label21.TabIndex = 47;
            this.label21.Text = ":";
            // 
            // label3_m
            // 
            this.label3_m.BackColor = System.Drawing.Color.Transparent;
            this.label3_m.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3_m.ForeColor = System.Drawing.Color.White;
            this.label3_m.Location = new System.Drawing.Point(497, 9);
            this.label3_m.Name = "label3_m";
            this.label3_m.Size = new System.Drawing.Size(52, 34);
            this.label3_m.TabIndex = 48;
            this.label3_m.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // FormLevel3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(645, 501);
            this.ControlBox = false;
            this.Controls.Add(this.label3_box3);
            this.Controls.Add(this.label3_box2);
            this.Controls.Add(this.label3_box4);
            this.Controls.Add(this.label3_box1);
            this.Controls.Add(this.label3_box5);
            this.Controls.Add(this.label3_key);
            this.Controls.Add(this.label3_wall1);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label3_wall3);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3_finish);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3_door);
            this.Controls.Add(this.label3_wall2);
            this.Controls.Add(this.label3_start);
            this.Controls.Add(this.label3_s);
            this.Controls.Add(this.label3_m);
            this.Controls.Add(this.label21);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormLevel3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Shown += new System.EventHandler(this.FormLevel3_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label3_start;
        private System.Windows.Forms.Label label3_finish;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label3_wall3;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label3_wall1;
        private System.Windows.Forms.Label label3_wall2;
        private System.Windows.Forms.Label label3_key;
        private System.Windows.Forms.Label label3_door;
        private System.Windows.Forms.Label label3_box5;
        private System.Windows.Forms.Label label3_box1;
        private System.Windows.Forms.Label label3_box4;
        private System.Windows.Forms.Label label3_box2;
        private System.Windows.Forms.Label label3_box3;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label3_s;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label3_m;
        private System.Windows.Forms.Timer timer2;
    }
}