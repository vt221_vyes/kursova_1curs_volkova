﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Labirint2D
{
    public partial class FormLevel3 : System.Windows.Forms.Form
    {
        int left3_boxes;
        public FormLevel3()
        {
            InitializeComponent();
        }
        private void start_game()
        {
            MessageBox.Show("У вас є 30 секунд щоб пройти цей рівень. По закінченню цього часу фініш зникне і ви не зможете пройти лабіринт");
            Point point;
            point = label3_start.Location;
            point.Offset(label3_start.Width / 2, label3_start.Height / 2);
            Cursor.Position = PointToScreen(point);
            left3_boxes = 5;
            label3_box1.Visible = true;
            label3_box2.Visible = true;
            label3_box3.Visible = true;
            label3_box4.Visible = true;
            label3_box5.Visible = true;
            label3_wall1.Visible = true;
            label3_wall2.Visible = false;
            label3_wall3.Visible = true;
            label3_key.Visible = true;
            label3_door.Visible = true;
            timer1.Enabled = true;
            timer2.Enabled = true;
            label3_finish.Visible = true;
            
        }
        
        private void finish_game()
        {
            
            DialogResult dr = MessageBox.Show("Ви програли, спробуйте ще раз!", "Повідомлення", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
            if (dr == System.Windows.Forms.DialogResult.Yes)
                start_game();
            else
                DialogResult = System.Windows.Forms.DialogResult.Abort;
        }

        private void FormLevel3_Shown(object sender, EventArgs e)
        {
            start_game();
        }

        private void label2_MouseEnter(object sender, EventArgs e)
        {
            finish_game();
        }



        private void label3_finish_MouseEnter(object sender, EventArgs e)
        {
            timer1.Enabled = false;
                if (left3_boxes == 0)
                {
                    if (i_m > 0)
                    {
                        DialogResult = System.Windows.Forms.DialogResult.OK;
                        MessageBox.Show("Ви перемогли! Час: " + label3_m.Text + "xв" + label3_s.Text + "c", "");
                    }
                    else
                    {
                        DialogResult = System.Windows.Forms.DialogResult.OK;
                        MessageBox.Show("Ви перемогли! Час: " + label3_s.Text + "с", "");
                    }
                }
                else
                    MessageBox.Show("Ви зібрали не всі кубики!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            label3_wall1.Visible = !label3_wall1.Visible;
            label3_wall2.Visible = !label3_wall2.Visible;
            label3_wall3.Visible = !label3_wall3.Visible;

        }

        private void label3_door_MouseEnter(object sender, EventArgs e)
        {
            if (label3_key.Visible)
                MessageBox.Show("Потрібен ключ!", "-", MessageBoxButtons.OK);
            else
            {
                label3_door.Visible = false;
               
            }
        }
        private void label3_box1_MouseEnter(object sender, EventArgs e)
        {
            left3_boxes--;
            ((Label)sender).Visible = false;
        }

        private void label3_key_MouseEnter(object sender, EventArgs e)
        {
            label3_key.Visible = false;
        }

        int i_s,i_m, i=30;

        

        private void timer2_Tick(object sender, EventArgs e)
        {
            i--;
            if (i == 0)
            {
                label3_finish.Visible = false;
                finish_game();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            i_s++;
            if (i_s <= 60)
            {

                label3_s.Text = i_s.ToString();
            }
            if (i_s % 60 == 0)
            {
                i_m++;
                label3_m.Text = i_m.ToString();
                i_s = 0;
                label3_s.Text = i_s.ToString();
            }
        }
    }
}
