﻿
namespace Labirint2D
{
    partial class FormLevel1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label_start = new System.Windows.Forms.Label();
            this.label_finish = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label_box1 = new System.Windows.Forms.Label();
            this.label_box2 = new System.Windows.Forms.Label();
            this.label_box4 = new System.Windows.Forms.Label();
            this.label_box5 = new System.Windows.Forms.Label();
            this.label_box3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label_m = new System.Windows.Forms.Label();
            this.label_s = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_start
            // 
            this.label_start.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label_start.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_start.Location = new System.Drawing.Point(5, 6);
            this.label_start.Name = "label_start";
            this.label_start.Size = new System.Drawing.Size(115, 48);
            this.label_start.TabIndex = 0;
            this.label_start.Text = "start";
            this.label_start.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_finish
            // 
            this.label_finish.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label_finish.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_finish.Location = new System.Drawing.Point(533, 496);
            this.label_finish.Name = "label_finish";
            this.label_finish.Size = new System.Drawing.Size(121, 42);
            this.label_finish.TabIndex = 1;
            this.label_finish.Text = "finish";
            this.label_finish.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_finish.MouseEnter += new System.EventHandler(this.label_finish_MouseEnter);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Maroon;
            this.label2.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(0, -4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 552);
            this.label2.TabIndex = 2;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.MouseEnter += new System.EventHandler(this.label4_MouseEnter);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Maroon;
            this.label3.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(655, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(10, 548);
            this.label3.TabIndex = 3;
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.MouseEnter += new System.EventHandler(this.label4_MouseEnter);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Maroon;
            this.label4.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(0, -4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(665, 10);
            this.label4.TabIndex = 4;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.MouseEnter += new System.EventHandler(this.label4_MouseEnter);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Maroon;
            this.label5.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(0, 538);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(665, 10);
            this.label5.TabIndex = 5;
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label5.MouseEnter += new System.EventHandler(this.label4_MouseEnter);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Maroon;
            this.label6.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(207, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 364);
            this.label6.TabIndex = 6;
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label6.MouseEnter += new System.EventHandler(this.label4_MouseEnter);
            // 
            // label_box1
            // 
            this.label_box1.BackColor = System.Drawing.Color.Yellow;
            this.label_box1.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_box1.Location = new System.Drawing.Point(91, 140);
            this.label_box1.Name = "label_box1";
            this.label_box1.Size = new System.Drawing.Size(29, 29);
            this.label_box1.TabIndex = 7;
            this.label_box1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_box1.MouseEnter += new System.EventHandler(this.label_box5_MouseEnter);
            // 
            // label_box2
            // 
            this.label_box2.BackColor = System.Drawing.Color.Yellow;
            this.label_box2.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_box2.Location = new System.Drawing.Point(324, 297);
            this.label_box2.Name = "label_box2";
            this.label_box2.Size = new System.Drawing.Size(29, 29);
            this.label_box2.TabIndex = 8;
            this.label_box2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_box2.MouseEnter += new System.EventHandler(this.label_box5_MouseEnter);
            // 
            // label_box4
            // 
            this.label_box4.BackColor = System.Drawing.Color.Yellow;
            this.label_box4.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_box4.Location = new System.Drawing.Point(592, 320);
            this.label_box4.Name = "label_box4";
            this.label_box4.Size = new System.Drawing.Size(29, 29);
            this.label_box4.TabIndex = 9;
            this.label_box4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_box4.MouseEnter += new System.EventHandler(this.label_box5_MouseEnter);
            // 
            // label_box5
            // 
            this.label_box5.BackColor = System.Drawing.Color.Yellow;
            this.label_box5.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_box5.Location = new System.Drawing.Point(427, 62);
            this.label_box5.Name = "label_box5";
            this.label_box5.Size = new System.Drawing.Size(29, 29);
            this.label_box5.TabIndex = 10;
            this.label_box5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_box5.MouseEnter += new System.EventHandler(this.label_box5_MouseEnter);
            // 
            // label_box3
            // 
            this.label_box3.BackColor = System.Drawing.Color.Yellow;
            this.label_box3.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_box3.Location = new System.Drawing.Point(133, 457);
            this.label_box3.Name = "label_box3";
            this.label_box3.Size = new System.Drawing.Size(29, 29);
            this.label_box3.TabIndex = 11;
            this.label_box3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_box3.MouseEnter += new System.EventHandler(this.label_box5_MouseEnter);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Maroon;
            this.label1.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(424, 234);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 305);
            this.label1.TabIndex = 12;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.MouseEnter += new System.EventHandler(this.label4_MouseEnter);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label_m
            // 
            this.label_m.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label_m.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_m.Location = new System.Drawing.Point(489, 20);
            this.label_m.Name = "label_m";
            this.label_m.Size = new System.Drawing.Size(52, 34);
            this.label_m.TabIndex = 13;
            this.label_m.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_s
            // 
            this.label_s.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label_s.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_s.Location = new System.Drawing.Point(575, 20);
            this.label_s.Name = "label_s";
            this.label_s.Size = new System.Drawing.Size(52, 34);
            this.label_s.TabIndex = 14;
            this.label_s.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(547, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(22, 34);
            this.label9.TabIndex = 15;
            this.label9.Text = ":";
            // 
            // FormLevel1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(663, 548);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label_s);
            this.Controls.Add(this.label_m);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_box3);
            this.Controls.Add(this.label_box5);
            this.Controls.Add(this.label_box4);
            this.Controls.Add(this.label_box2);
            this.Controls.Add(this.label_box1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label_start);
            this.Controls.Add(this.label_finish);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormLevel1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Shown += new System.EventHandler(this.FormLevel1_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label_start;
        private System.Windows.Forms.Label label_finish;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label_box1;
        private System.Windows.Forms.Label label_box2;
        private System.Windows.Forms.Label label_box4;
        private System.Windows.Forms.Label label_box5;
        private System.Windows.Forms.Label label_box3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label_m;
        private System.Windows.Forms.Label label_s;
        private System.Windows.Forms.Label label9;
    }
}