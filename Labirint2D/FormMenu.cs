﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace Labirint2D
{
    public partial class FormMenu : System.Windows.Forms.Form
    {
        public FormMenu()
        {
            InitializeComponent();
        }

        private void button_start_Click(object sender, EventArgs e)
        {   
            start_level1();
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void start_level1()
        {
            FormLevel1 level1 = new FormLevel1();
            DialogResult dr = level1.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
                start_level2();
        }

        private void start_level2()
        {
            FormLevel2 level2 = new FormLevel2();
            DialogResult dr = level2.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                start_level3();
            }
        }
        private void start_level3()
        {
           
            FormLevel3 level3 = new FormLevel3();
            DialogResult dr = level3.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                
                start_youwin();
            }
        }

        private void start_youwin()
        {
            MessageBox.Show("Ви пройшли лабіринт!");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Гра складається з трьох рівнів. На кожному рівні є ускладнення. " +
                "Ви не можете перейти до наступного рівня поки не подолаєте перешкоди та не зберете всі потрібні кубики. " +
                "Уникайте мигаючих стінок, зіткнення з ними призведе до програшу на рівні.", "Правила", MessageBoxButtons.OK,MessageBoxIcon.Information);
        }
    }
}
