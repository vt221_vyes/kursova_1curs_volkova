﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Labirint2D
{
    public partial class FormLevel2 : System.Windows.Forms.Form
    {
        public FormLevel2()
        {
            InitializeComponent();
        }
        private void start_game()
        {
            Point point;
            point = label2_start.Location;
            point.Offset(label2_start.Width / 2, label2_start.Height / 2);
            Cursor.Position = PointToScreen(point);
           
            label_key.Visible = true;
            label_door.Visible = true;
            label_wall1.Visible = true;
            label_wall2.Visible = false;
        }
        private void finish_game()
        {
            
            DialogResult dr = MessageBox.Show("Ви програли, спробуйте ще раз!", "Повідомлення", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
            if (dr == System.Windows.Forms.DialogResult.Yes)
                start_game();
            else
                DialogResult = System.Windows.Forms.DialogResult.Abort;
        }

        private void FormLevel2_Shown(object sender, EventArgs e)
        {
            start_game();
        }

        private void label2_finish_MouseEnter(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            if (i_m > 0)
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
                MessageBox.Show("Ви пройшли 2 рівень! Час: " + label2_m.Text + "хв" + label2_s.Text + "с Перейти до фінального?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }
            else
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
                MessageBox.Show("Ви пройшли 2 рівень! Час: " + label2_s.Text + "с Перейти до фінального?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }
            }

        private void label1_MouseEnter(object sender, EventArgs e)
        {
            finish_game();
        }

        private void label_key_MouseEnter(object sender, EventArgs e)
        {
            label_key.Visible = false;
           
        }

        private void label_door_MouseEnter(object sender, EventArgs e)
        {
            if (label_key.Visible)
                MessageBox.Show("Потрібен ключ!", "-", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                label_door.Visible = false;
                
            }
            
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            label_wall1.Visible = !label_wall1.Visible;
            label_wall2.Visible = !label_wall2.Visible;
        }
        int i_m, i_s;

        private void timer1_Tick(object sender, EventArgs e)
        {
            i_s++;
            if (i_s <= 60)
            {

                label2_s.Text = i_s.ToString();
            }
            if (i_s % 60 == 0)
            {
                i_m++;
                label2_m.Text = i_m.ToString();
                i_s = 0;
                label2_s.Text = i_s.ToString();
            }
        }
    }
}
