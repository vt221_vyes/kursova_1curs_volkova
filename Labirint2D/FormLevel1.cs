﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Labirint2D
{
    public partial class FormLevel1 : System.Windows.Forms.Form
    {
       
        int left_boxes;

        public FormLevel1()
        {
            InitializeComponent();
        }
        private void start_game()
        {
            Point point;
            point = label_start.Location;
            point.Offset(label_start.Width / 2, label_start.Height / 2);
            Cursor.Position = PointToScreen( point);
            left_boxes = 5;
            label_box1.Visible = true;
            label_box2.Visible = true;
            label_box3.Visible = true;
            label_box4.Visible = true;
            label_box5.Visible = true;
            timer1.Enabled = true;
            
        }
        private void finish_game()
        {
            
            
            M = i_m; S = i_s;
            DialogResult dr = MessageBox.Show("Ви програли, спробуйте ще раз!", "Повідомлення", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
            if (dr == System.Windows.Forms.DialogResult.Yes)
                start_game();
            else
                DialogResult = System.Windows.Forms.DialogResult.Abort;
        }

        private void FormLevel1_Shown(object sender, EventArgs e)
        {
            start_game();
        }

        private void label_finish_MouseEnter(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            if (left_boxes == 0)
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
                i_m.ToString(); i_s.ToString();
                if(i_m>0)
                MessageBox.Show("Ви пройшли 1 рівень! " +
                    "Час: "+label_m.Text+"хв "+label_s.Text+"c"+" Перейти до наступного?","", MessageBoxButtons.YesNo,MessageBoxIcon.Question);
                if(i_m==0)
                    MessageBox.Show("Ви пройшли 1 рівень! " +
                    "Час: " + label_s.Text + "c" + " Перейти до наступного?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }
            else
                MessageBox.Show("Ви зібрали не всі кубики!","", MessageBoxButtons.OK ,MessageBoxIcon.Error);
        }

        private void label4_MouseEnter(object sender, EventArgs e)
        {
            finish_game();
            i_s = 0;
        }

        private void label_box5_MouseEnter(object sender, EventArgs e)
        {
            
            left_boxes--;
            ((Label)sender).Visible = false;
        }

        int i_m, i_s, M, S ;
        private void timer1_Tick(object sender, EventArgs e)
        {
            i_s++;
            if (i_s <= 60)
            {
                
                label_s.Text = i_s.ToString();
            }
            if (i_s % 60==0)
            {
                i_m++ ;
                label_m.Text = i_m.ToString();
                i_s = 0;
                label_s.Text = i_s.ToString();
            }
           
        }
       
    }
}
